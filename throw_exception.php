<?php

function fancyCalculation($number) {
	if(!is_numeric($number)) {
		throw new Exception('$number should be an integer!');
	}
	return $number * 2;
}

$number = <<<HTML
 hello world;
HTML;

try {

	$number = fancyCalculation($number);
	var_dump($number);
} catch(Exception $e) {
	date_default_timezone_set('America/Costa_Rica');
	$date = date('M-d-Y h:i:s');

	$newline = __FILE__ . __LINE__ . PHP_EOL;
	$file = 'logs/error_log.txt';

	$log = <<<ERROR
	{$date} => {$e->getMessage()} => {$newline}
ERROR;

	$records = file_get_contents($file);
	$new = $records . $log;
	file_put_contents($file, 
		$new
	);
}

var_dump(
	file_get_contents($file)
);
