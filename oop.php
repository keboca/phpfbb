<?php

/**
 * Class Vehicule
 */
abstract class Vehicule implements Engine {

    /** @var int */
    protected $tires;

    /**
     * Vehicule constructor.
     * @param $tires
     */
    public function __construct($tires = 4)
    {
        $this->setTires($tires);
    }

    /**
     * @return int
     */
    public function getTires(): int
    {
        return $this->tires;
    }

    /**
     * @param int $tires
     */
    public function setTires(int $tires)
    {
        $this->tires = $tires;
    }
}

/** Class Car */
class Car extends Vehicule {
    public function start() {
        print __CLASS__ . ' started!';
    }
}

/**
 * Class Motorcycle
 */
class Motorcycle extends Vehicule {

    /**
     * Motorcycle constructor.
     * @param int $tires
     */
    public function __construct($tires = 2)
    {
        parent::__construct($tires);
        $this->start();
    }

    public function start() {
        print get_class($this) . ' started!';
    }
}

/**
 * Class Monocycle
 */
class Monocycle extends Motorcycle {

    /**
     * Monocycle constructor.
     */
    public function __construct()
    {
        parent::__construct(1);
    }
}

/**
 * Interface Engine
 */
interface Engine {

    /**
     * @return void
     */
    public function start();
}

$hyndai = new Car();
var_dump(
    new Motorcycle(),
    new Monocycle(),
    $hyndai->start()
);
