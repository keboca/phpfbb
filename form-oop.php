<?php

class FormOop {

	/** @var array */
	protected $request;

	/** @var bool */
	protected $valid;

	public function __construct($request) {
		$this->request = $request;
	}

	public function isValid(): bool {
		return $this->valid;
	}

	public function validate(): array {
		$inputs = [
			empty($this->request['first_name']),
			empty($this->request['last_name']),
			!isset($this->request['sick']),
		];

		$this->valid = empty(array_filter($inputs));
		return $inputs;
	}

	public function render(): string {
		$first_name = $this->request['first_name'] ?? NULL;
		$last_name = !empty($this->request['last_name']) ? 
			$this->request['last_name'] : NULL;

		$am_I_sick = NULL;
		if(isset($this->request['sick'])) {

			$am_I_sick = 'Pura Vida!';
			if( (bool) $this->request['sick'] ) {
				$am_I_sick = 'Acetaminofen...';
			}
		}


		$markup = <<<HTML
			<div id="result">
				<h3>Welcome</h3>
				<hr/>
				<b> {$first_name} {$last_name} </b>
				&nbsp;<i> {$am_I_sick} </i>
			</div>
HTML;

		return $markup;
	}

}

// $form = new FormOop(['foo' => 'bar']);
// var_dump($form->validate());
?>