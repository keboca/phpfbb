<div class="col-md-12 col-lg-6 text-left text-lg-right" data-aos="fade-up" data-aos-delay="100">
<div id="filters" class="filters">
  <a href="#" data-filter="*" class="active">All</a>
  <?php 

  $filters = [
  	[
  		'title' => 'Web',
  		'filter' => '.web',	
  	],
  ];
// <a href="#" data-filter=".web">Web</a>
//   <a href="#" data-filter=".design">Design</a>
//   <a href="#" data-filter=".branding">Branding</a>
//   <a href="#" data-filter=".photography">Photography</a>

  foreach ($filters as $filter) {
  	echo "<a href='#' data-filter='{$filter['filter']}'>{$filter['title']}</a>";
  }
  ?>
</div>
</div>