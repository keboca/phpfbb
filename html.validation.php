<html>
	<head>
		<title>PHP - Foo Bar Baz</title>
	</head>
	<body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript">

        function calcAvgHeight(data) {
            // Extract data or defined default value.
            var data = data || {};

            // Init variables.
            var i = 0, total = 0;

            // Sum height values from each object.
            for (let key in data) {
                total += data[key].height;
                i++;
            }

            // When there is not object, then return null.
            if(0 === i) return null;

            // Calculate avg. value.
            return (total / i);
        }

        var avgHeight = calcAvgHeight({
            Matt: { height: 174, weight: 69 },
            Jason: { height: 190, weight: 103 }
        });
        console.log(avgHeight);

        function validateInput() {
    $('.error-message').html('');

    $satisfied = $('input[name=satisfied]:checked');
    $donate = $('input[name=donate]');
    $reason = $('input[name=reason]');

    if("0" === $satisfied.val()) {

        var message = null;
        if($donate.is(':checked')) {
            message = 'RULE 1: FAILED.';
        }

        if($reason.val() === '') {
            message = message ? 'BOTH RULES FAILED.' : 'RULE 2: FAILED.';
        }

        $(".error-message").html(message);
        if(message !== null) {
            return false;
        }
    }

    return true;
}

        document.body.innerHTML = `
        <!-- Display error message in following div -->
        <div class="error-message"></div>

        <form id="satisfaction" onsubmit="return validateInput()">
        <input type="radio" name="satisfied" value="1" required checked="checked" /> Satisfied
        <input type="checkbox" name="donate" /> Donate<br />
        <input type="radio" name="satisfied" value="0" required /> Not satisfied
        <input type="text" name="reason" /> Reason<br />
        <input type="submit" value="Submit" />
        </form>`;

    </script>


	</body>
</html>