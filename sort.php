<?php

$colors = array(
		[
			'id' => 2, 
			'name' => 'black'
		], 
		[
			'id' => 3, 
			'name' => 'blue'
		], 
		[ 
			'id' => 1, 
			'name' => 'red'
		] 
	);

	echo PHP_EOL . "----------Sin ordenar----------";
	foreach ($colors as $key => $row) {
		print_r(PHP_EOL . $row['id'] . ' --> ' . $row['name']);
	}
	echo PHP_EOL . "----------Sin ordenar----------" . PHP_EOL;
/*
	Aquí inicia el proceso de ordenamiento de los vectores.
*/
	for ($i=0; $i <= count($colors)-1; $i++) { 

		for ($j= $i+1; $j <= count($colors)-1; $j++) { 

			if($colors[$i]['id'] > $colors[$j]['id']){ //Este if permite el ordenamiento del arreglo de manera ASC.


				$auxID = $colors[$i]['id'];
				$colors[$i]['id'] = $colors[$j]['id'];
				$colors[$j]['id'] = $auxID;

				$auxCOL = $colors[$i]['name'];
				$colors[$i]['name'] = $colors[$j]['name'];
				$colors[$j]['name'] = $auxCOL;
			}
		}
	}
/*
	Aquí finaliza el proceso de ordenamiento de los vectores.
*/
	echo PHP_EOL . "------------Ordenado-----------";
	foreach ($colors as $key => $row) {
		print_r(PHP_EOL . $row['id'] . ' --> ' . $row['name']);
	}
	echo PHP_EOL . "------------Ordenado-----------" . PHP_EOL . PHP_EOL;