<?php

$first_name = $_REQUEST['first_name'] ?? NULL;
$last_name = !empty($_REQUEST['last_name']) ? $_REQUEST['last_name'] : NULL;

$am_I_sick = NULL;
if(isset($_REQUEST['sick'])) {

	$am_I_sick = 'Pura Vida!';
	if( (bool) $_REQUEST['sick'] ) {
		$am_I_sick = 'Acetaminofen...';
	}
}


$markup = <<<HTML
	<div id="result">
		<h3>Welcome</h3>
		<hr/>
		<b> {$first_name} {$last_name} </b>
		&nbsp;<i> {$am_I_sick} </i>
	</div>
HTML;

print $markup;
