<?php

# Sort the following array based on its `id` key: array( [id => 2, name => black], [id => 3, name => blue], [ id => 1, name => red] )
$try = array(
	['id' => 2, 'name' => 'black'],
	['id' => 3, 'name' => 'blue'], 
	[ 'id' => 1, 'name' => 'red'],
);

usort($try, function($alpha, $beta) {
	if($alpha['id'] == $beta['id']) {
		return 0;
	}

	return ($alpha['id'] > $beta['id']) ? 1 : -1;
});
print_r($try);

// // asort(); ksort(); usort();
// print_r($try);

# Retrieve keys from array [ one => 1, two => 2] then combine with the values [ jan, feb ]
// $first = [ 'one' => 100, 'two' => 234];
// $keys = array_keys($first);

// $second = ['jan', 'feb'];

// $combined = array_merge($second, $keys, $first, array( 'nuevo', 23 ));
// print_r($combined);
