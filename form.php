<!DOCTYPE html>
<html>
<head>
	<title>My Form</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
	<?php

		$valid_first_name = $valid_last_name = $valid_sick = NULL;
		if('POST' == $_SERVER['REQUEST_METHOD']) {
			include 'form-oop.php';

			$form = new FormOop($_REQUEST);
			list($valid_first_name,$valid_last_name, $valid_sick) = $form->validate();
		}
	?>

<?php if(empty($form) || isset($form) && !$form->isValid()): ?>

	<form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="post">
		<label for="first_name">First name:</label>
		<input type="type" name="first_name" id="first_name"/>
		
		<?php if($valid_first_name): ?>

			<div class="alert alert-danger" role="alert">
			  Please, provide us a name.
			</div>

		<?php endif; ?>

		<br/>
		<label for="last_name">Last name:</label>
		<input type="type" name="last_name" id="last_name"/>

		<?php if($valid_last_name): ?>

			<div class="alert alert-danger" role="alert">
			  Please, provide us a last name.
			</div>

		<?php endif; ?>
		<br/>
		<input type="radio" name="sick" id="yes_sick" value="1">
		<label for="yes_sick">Yup, I'm sick.</label>
		<br/>
		<input type="radio" name="sick" id="no_sick" value="0">
		<label for="no_sick">Nope, I'm okay.</label>

		<?php if($valid_sick): ?>

			<div class="alert alert-danger" role="alert">
			  Please, choose an option.
			</div>

		<?php endif; ?>
		<br/>
		<input type="submit" value="Send"/>
		<input type="reset" name="Clear" style="display: none;" />
	</form>

<?php else: ?>

	<?php print $form->render(); ?>

<?php endif; ?>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>