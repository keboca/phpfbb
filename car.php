<?php

class Car extends Vehicule {
	protected $doors;
	private $counter = 0;

	public function __construct($tires, $doors, $color = 'grey') {
		$this->setTires($tires);
		$this->setDoors($doors);
		$this->setColor($color);
	}

	public function setTires($tires) {
		if(4 == $tires) {
			$tires++;
		}
		$this->tires = $tires;
	}

	public function setDoors($doors) {
		$this->doors = $doors;
	}

	public function getDoors() {
		return $this->doors;
	}

	public function resume() {
		return printf(
			'Este nuevo modelos tiene: %s llantas. %s puertas y de color: %s',
			$this->getTires(),
			$this->getDoors(),
			$this->getColor()
		);
	}
}

class Truck extends Car {

	public function __construct() {
		parent::__construct(18, 2);
	}

	public function __toString() {
		$class = __CLASS__;
		$legend = <<<TEXT
		My super truck {$class} is awesome! It has {$this->getTires()} tires!
TEXT;
		return $legend;
	}
}

class Motorcycle extends Vehicule {

	public function __construct() {
		$this->color = 'pink';
	}
}

abstract class Vehicule {
	protected $tires;
	protected $color;

	public function getTires() {
		return $this->tires;
	}
	public function getColor() {
		return $this->color;
	}

	public function setTires($tires) {
		$this->tires = $tires;
	}
	public function setColor($color) {
		$this->color = $color;
	}

	public function __toString() {
		return __CLASS__ . PHP_EOL;
	}
}

class MonoBykeMotor extends Motorcycle {

	public function __construct() {
		parent::__construct();
		$this->tires = 1;
	}
}

$zsmiedo = new Motorcycle();
echo $zsmiedo;

// $volvo = new Truck();
// echo $volvo;
// $toyota = new Car(4,4);
// echo $toyota->resume();
// var_dump($toyota);
// var_dump($zsmiedo->getColor());


